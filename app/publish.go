package app

import (
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"

	"github.com/fsnotify/fsnotify"
)

func Release() {
	err := RunCommand(globalConfig.Publish.Release)
	if err != nil {
		log.Fatal(err)
	}
	MLog(globalConfig.Publish.Release)
}
func Sync() {
	dynamic, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}
	go func() {
		for {
			select {
			case event := <-dynamic.Events:
				if event.Op == fsnotify.Write {
					if event.Name == ".DS_Store" {
						continue
					}
					MLog(event.Name)
					 _ = RunCommand(globalConfig.Publish.Sync)
				}
			case err := <-dynamic.Errors:
				MFatal(err.Error)
			}
		}
	}()
	var dirs = []string{
		filepath.Join(rootPath, "source"),
	}
	var files = []string{
		filepath.Join(themePath),
	}

	for _, source := range dirs {
		_ = SymWalk(source, func(path string, f os.FileInfo, err error) error {
			if f.IsDir() {
				if err := dynamic.Add(path); err != nil {
					MFatal(err.Error)
				}
			}

			return nil
		})
	}

	for _, source := range files {
		if err := dynamic.Add(source); err != nil {
			MFatal(err.Error())
		}
	}
}

func RunCommand(command string) error {
	var shell, flag string
	if runtime.GOOS == "windows" {
		shell = "cmd"
		flag = "/C"
	} else {
		shell = "/bin/sh"
		flag = "-c"
	}
	cmd := exec.Command(shell, flag, command)
	stdout, _ := cmd.StdoutPipe()
	stderr, _ := cmd.StderrPipe()
	if err := cmd.Start(); err != nil {
		log.Printf("Error starting command: %s......", err.Error())
		return err
	}
	go rsyncLog(stdout)
	go rsyncLog(stderr)
	if err := cmd.Wait(); err != nil {
		log.Printf("Error waiting for command execution: %s......", err.Error())
		return err
	}
	return nil
}
func rsyncLog(reader io.ReadCloser) {
	cache := "" // 缓存不足一行的日志信息
	buf := make([]byte, 1024)
	for {
		num, err := reader.Read(buf)
		if err != nil && err != io.EOF {
			panic(err)
		}
		if num > 0 {
			b := buf[:num]
			s := strings.Split(string(b), "\n")
			line := strings.Join(s[:len(s)-1], "\n") // 取出整行的日志
			fmt.Printf("%s%s\n", cache, line)
			cache = s[len(s)-1]
		}
	}
}
