package app

import (
	"fmt"
	"os"
)

const (
	ClrR = "\x1b[31;1m"
	ClrY = "\x1b[33;1m"
)

// 自定信息输出
func MLog(info interface{}) {
	fmt.Printf("%s\n", info)
}

// 打印警告不退出
func MWarn(info interface{}) {
	fmt.Printf("WARNING: %s%s\n%s", ClrY, info, "\x1b[0m")
}

// 只打印错误不退出
func MError(info interface{}) {
	fmt.Printf("ERR: %s%s\n%s", ClrR, info, "\x1b[0m")
}

// 打印错误日志并退出
func MFatal(info interface{}) {
	MError(info)
	os.Exit(1)
}
