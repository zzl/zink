package app

import (
	"path/filepath"
)

func Serve() {
	service := NewHttpServer()
	service.Get("/websocket", Websocket)
	service.Get("*", Static(filepath.Join(rootPath, globalConfig.Build.Output)))
	uri := "http://localhost:" + globalConfig.Build.Port + "/"
	MLog("Service running " + uri)
	service.Listen(":" + globalConfig.Build.Port)
}
