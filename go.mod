module gitee.com/zzl/zink

go 1.14

require (
	github.com/fsnotify/fsnotify v1.4.9
	github.com/gorilla/feeds v1.1.1
	github.com/gorilla/websocket v1.4.1
	github.com/russross/blackfriday/v2 v2.0.1
	github.com/urfave/cli/v2 v2.2.0
	gopkg.in/yaml.v2 v2.2.8
)
