package main

import (
	"os"

	"gitee.com/zzl/zink/app"
	"github.com/urfave/cli/v2"
)

/*
函数（功能）入口，也是命令入口 ---> Meink run ,Meink init ....
*/
func main() {
	cmd := &cli.App{
		Name:    app.Name,
		Usage:   app.Usage,
		Version: app.Version,
		Authors: []*cli.Author{
			{Name: app.Author, Email: app.AuthorEmail},
		},
		Commands: []*cli.Command{
			{
				Name:  "run",
				Usage: "运行博客",
				Action: func(c *cli.Context) error {
					app.ParseGlobalConfigForWrap(true)
					app.Build()
					app.DynamicMonitoringFile()
					app.Serve()
					return nil

				},
			},
			{
				Name:  "init",
				Usage: "清空public文件夹(用于调试)",
				Action: func(c *cli.Context) error {
					app.ParseGlobalConfigForWrap(false)
					app.CleanPublic()
					return nil

				},
			},
			{
				Name:  "release",
				Usage: "发布版打包到release文件夹",
				Action: func(c *cli.Context) error {
					app.ParseGlobalConfigForWrap(false)
					app.Release()
					return nil
				},
			},
			{
				Name:  "new",
				Usage: "创建一篇新文章 --> 创建文章用 new article_name  创建页面用 new page_name page ",
				Action: func(c *cli.Context) error {
					app.ParseGlobalConfigForWrap(false)
					app.NewArticle(c)
					app.Sync()
					app.Serve()
					return nil
				},
			},
			{
				Name:  "sync",
				Usage: "同步文章到服务器",
				Action: func(c *cli.Context) error {
					app.ParseGlobalConfigForWrap(false)
					app.Sync()
					app.Serve()
					return nil
				},
			},
		},
	}
	if err := cmd.Run(os.Args); err != nil {
		panic(err)
	}
}
